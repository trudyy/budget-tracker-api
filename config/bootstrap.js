/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.bootstrap.html
 */

var Promise = require('bluebird');

module.exports.bootstrap = function(cb) {

  var _user = User.findOne({username: 'trudywong'})
    .then(function(user){
      if (!user) {
        return User.create(
          {
            username: 'trudywong',
            email: 'trudypwong@gmail.com',
            password: 'testing',
            weeklyLimit: 30.00,
            monthlyLimit: 100.00
          })
          .then(function(result){
            console.log('Creating user');
            return result;
          })
      } else {
        console.log('Already exists user');
        return user;
      }
    })
    .catch(function(err){
      sails.log.error(err)
    });

  var _expense = true;
  // var _expense = Expense.findOne({description: 'Mocha', user: 1})
  //   .then(function(expense){
  //     if (!expense) {
  //       return Expense.create({description: 'Mocha', user: 1, amount: 2.01, date: new Date()})
  //         .then(function(result){
  //           console.log('Creating expense');
  //           return result;
  //         })
  //     } else {
  //       console.log('Already exists expense');
  //       return expense;
  //     }
  //   })
  //   .catch(function(err){
  //     sails.log.error(err)
  //   });

  Promise
    .all([_user, _expense])
    .spread(function(user, expense){
      console.log('Completed bootstrap');
      cb();
    })
    .catch(function(err){
      sails.log.error(err)
    });

  // It's very important to trigger this callback method when you are finished
  // with the bootstrap!  (otherwise your server will never lift, since it's waiting on the bootstrap)
};
