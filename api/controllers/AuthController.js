/**
 * AuthController
 *
 * @description :: Server-side logic for managing auths
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var passport = require('passport');

module.exports = {

  /**
   * `AuthController.login()`
   */
  login: function (req, res) {
    var params = req.allParams();
    sails.log.info('auth/login');
    sails.log.info(params);

    if (!params.email || !params.password) {
      return res.badRequest({},"Missing credentials for login");
    }

    passport.authenticate('local', function (err, user, info) {
      if (err) {
        return res.badRequest(err, "Error During Login");
      }
      if (!user) {
        var message = (info && info.message) ? info.message : "No account with this email";
        return res.badRequest(err, message);
      }
      req.logIn(user, function(err) {
        if (err) {
          return res.badRequest(err, "Error During Login");
        }

        return User.findOne({id: user.id})
          .then(function(user){
            // Return object must match exactly object expected on android
            delete user.password;
            delete user.createdAt;
            delete user.updatedAt;
            delete user.weeklyLimit;
            delete user.monthlyLimit;
            user.token = TokenService.issue({id: user.id});
            console.log(user);
            return res.ok(user, "Login Successful");
            // return res.ok({user: user, token: TokenService.issue({id: user.id})}, "Login Successful");
          })
          .catch(function(err){
            sails.log.error(err);
            return res.badRequest(err, err.message);
          });

      });
    })(req, res);

  },

  /**
   * `AuthController.register()`
   */
  register: function (req, res) {
    var params = req.allParams();
    sails.log.info('auth/register');
    sails.log.info(params);

    if (!params.email) {
      return res.badRequest({},"Missing email");
    }
    if (!params.password) {
      return res.badRequest({},"Missing password");
    }
    if (!params.confirm) {
      return res.badRequest({},"Missing password confirmation");
    }
    if (params.confirm != params.password) {
      return res.badRequest({},"Passwords do not match");
    }
    if (params.password.length < 5) {
      return res.badRequest({},"Password must be 5 or more characters");
    }

    // First check that the email is available
    User.findOne({email: params.email})
      .then(function(user){
        if (!user) {
          return;
        } else {
          throw new Error("Email is not available")
        }
      })
      .then(function() {
        // Now create user
        return User.create(params)
          .then(function(user){
            if (user) {
              var result = user.toJSON();
              return res.ok({user: result}, "User created");
            } else {
              return res.badRequest({}, "User was not created")
            }
          })
          .catch(function(err){
            sails.log.error(err);
            return res.badRequest(err, err.message);
          })
      })
      .catch(function(err){
        sails.log.error(err);
        return res.badRequest(err, err.message);
      })
  },

  /**
   * `AuthController.signup()`
   */
  signup: function (req, res) {
    var params = req.allParams();
    sails.log.info('auth/signup');
    sails.log.info(params);

    if (!params.email || !params.password) {
      return res.badRequest({},"Missing information about user");
    }
    if (!params.weeklyLimit || !params.monthlyLimit) {
      return res.badRequest({},"Missing expense limit parameters");
    }

    // First check that the email is available
    User.findOne({email: params.email})
      .then(function(user){
        if (!user) {
          return;
        } else {
          throw new Error("Email is not available")
        }
      })
      .then(function() {
        // Now create user
        return User.create(params)
          .then(function(user){
            if (user) {
              var result = user.toJSON();
              return res.ok({user: result}, "User created");
            } else {
              return res.badRequest({}, "User was not created")
            }
          })
          .catch(function(err){
            sails.log.error(err);
            return res.badRequest(err, err.message);
          })
      })
      .catch(function(err){
        sails.log.error(err);
        return res.badRequest(err, err.message);
      })
  },

  /**
   * `AuthController.logout()`
   */
  logout: function (req, res) {
    sails.log.info('auth/logout');
    req.logout();
    return res.ok({}, "Logout Successful");
  }
};

