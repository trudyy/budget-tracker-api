/**
 * ExpenseController
 *
 * @description :: Server-side logic for managing expenses
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var moment = require('moment');

module.exports = {

  /**
   * Create a single expense
   * @param req
   * @param res
   * @returns {*}
   */
  create: function (req, res) {
    var params = req.allParams();
    sails.log.info('expense/create');
    sails.log.info(params);

    if (!params.user) {
      return res.badRequest({}, 'Missing user parameter');
    }
    if (!params.amount) {
      return res.badRequest({}, 'Missing amount parameter');
    }
    if (!params.description) {
      return res.badRequest({}, 'Missing description parameter');
    }
    if (!params.date) {
      return res.badRequest({}, 'Missing date parameter');
    }
    if (!moment(params.date).isValid()) {
      return res.badRequest({}, 'Wrong date format. Preferred format is yyyy-mm-dd');
    }

    Expense.create(params)
      .then(function(result) {
        if (result) {
          return res.created(result, 'Expense created');
        } else {
          throw new Error('Could not create expense');
        }
      })
      .catch(function(err) {
        sails.log.error(err);
        return res.badRequest(err, err.message);
      });
  },

  /**
   * Find an expense by it's id
   * @param req
   * @param res
   * @returns {*}
   */
  findOne: function (req, res) {
    var params = req.allParams();
    sails.log.info('expense/findOne');
    sails.log.info(params);

    if (!params.id) {
      return res.badRequest({}, 'Missing id parameter');
    }

    Expense.findOne({id: params.id})
      .then(function(result) {
        if (result) {
          return res.ok(result, 'Expense found');
        } else {
          throw new Error('Could not find the expense');
        }
      })
      .catch(function(err) {
        sails.log.error(err);
        return res.badRequest(err, err.message);
      });

  },

  /**
   * Find all expenses belonging to a user
   * @param req
   * @param res
   * @returns {*}
   */
  listAllByUser: function (req, res) {
    var params = req.allParams();
    sails.log.info('expense/listAllByUser');
    sails.log.info(params);

    if (!params.user) {
      return res.badRequest({}, 'Missing user parameter');
    }

    // MUST be in this format for android to parse - 2001-07-04T12:08:23PM
    var dateFormat = "YYYY-MM-DDThhmmssA";

    Expense.find({user: params.user})
      .then(function(result) {
        if (result && (result.length > 0)) {
          result = _.map(result, function(i){
            i.date = moment(new Date(i.date)).format(dateFormat);
            i.createdAt = moment(new Date(i.createdAt)).format(dateFormat);
            i.updatedAt = moment(new Date(i.updatedAt)).format(dateFormat);
            return i;
          });
          // console.log(result);
          return res.ok(result, 'Expenses belonging to the user are found');
        } else {
          return res.ok(result, 'No expenses found');
        }
      })
      .catch(function(err) {
        sails.log.error(err);
        return res.badRequest(err, err.message);
      });
  },

  syncDataForUser: function (req, res) {
    var params = req.allParams();
    sails.log.info('expense/syncDataForUser');
    sails.log.info(params);

    if (!params.id) {
      return res.badRequest({}, 'Missing user parameter');
    }

    // Expenses on the device
    var listExpenses = _.omit(params, "id");
    listExpenses = _.values(listExpenses);
    listExpenses = formatDates(listExpenses);
    // sails.log.info(listExpenses);

    // Track expenses
    var apiMatched = [];
    var apiMissing = [];
    var apiUpdate = [];
    var devMatched = [];
    var devMissing = [];

    // var returnFormat = "YYYY-MM-DDThhmmssA";       // 2001-07-04T12:08:23PM
    // var dateFormat = "YYYY-MM-DDTHHmmssz";        // 2001-07-04T15:08:23PDT
    // var dateFormat = "";                       // cannot just leave it (dev has offset -7)

    Expense.find({user: params.id})
      .then(function(result) {
        if (result && (result.length > 0)) {
          sails.log.info("------- Checking -------");
          return formatDates(result);
        } else {
          return result;
        }
      })
      .each(function(e){
        // Determine which api expenses need to be added to the DEVICE
        // sails.log.info("API Expense");

        // Loop through device's expense
        for (var i in listExpenses) {

          // If this expense already on the device
          if (listExpenses[i].id === e.id) {

            // Finding the device item that matches to the api item
            // var devCreatedAt = moment.utc(new Date(listExpenses[i].createdAt)).format(returnFormat);
            // var apiCreatedAt = moment.utc(new Date(e.createdAt)).format(returnFormat);
            var devCreatedAt = listExpenses[i].createdAt;
            var apiCreatedAt = e.createdAt;
            if (devCreatedAt === apiCreatedAt) {
              if (listExpenses[i].description === e.description) {
                if (listExpenses[i].amount === e.amount) {
                  // var devDate = moment.utc(new Date(listExpenses[i].date)).format(returnFormat);
                  // var apiDate = moment.utc(new Date(e.date)).format(returnFormat);
                  var devDate = listExpenses[i].date;
                  var apiDate = e.date;
                  if (devDate === apiDate) {
                    sails.log.info("This expense is ON device");
                    apiMatched.push(listExpenses[i]);
                    devMatched.push(listExpenses[i]);
                    return;
                  } else {
                    console.log("\tUpdate api item - changed on device");
                    console.log("\tdevDate: " + devDate+ "\n\tapiDate: " + apiDate);
                  }
                } else {
                  console.log("\tUpdate api item - changed on device");
                  console.log("\tdevAmount: " + listExpenses[i].amount +"\n\tapiAmount: " + e.amount);
                }
              } else {
                console.log("\tUpdate api item - changed on device");
                console.log("\tdevDesc: " + listExpenses[i].description +"\n\tapiDesc: " + e.description);
              }

              // Update api item - changed on device
              sails.log.info("This expense needs to be updated - update api");
              apiUpdate.push(listExpenses[i]);
              devMatched.push(listExpenses[i]);
              console.log(listExpenses[i]);
              return;

            } else {
              console.log("\tConflicting items - add this new item to api");
              console.log("\tdevCreatedAt: " + devCreatedAt +"\n\tapiCreatedAt: " + apiCreatedAt);

              // Conflicting items - add this new item to api
              sails.log.info("This expense is NOT on api yet");
              delete listExpenses[i].id;
              apiMissing.push(listExpenses[i]);
              devMatched.push(listExpenses[i]);
              // console.log(e);
              return;
            }
          }
        }

        // If this expense is not on the device yet
        devMissing.push(e);
        sails.log.info("This expense is NOT on device yet");
        console.log(e);
        return;
      })
      .then(function(){
        // find if any on device is not matched
        var devUnmatched = _.difference(listExpenses, devMatched);
        if (devUnmatched.length > 0) {
          sails.log.info("These expenses are not on the api yet");
          console.log(devUnmatched);
          apiMissing = _.union(apiMissing, devUnmatched);
        }
        return;
      })
      .then(function(){
        // expenses to add to db
        console.log("Expenses to add");
        console.log(apiMissing);
        return apiMissing;
      })
      .each(function(e) {
        var expense = formatDate(e);
        console.log("Adding DB expense");
        console.log(expense);
        return Expense.findOne({id: expense.id})
          .then(function(result) {
            if (result) {
              // change ids
              delete expense.id;
            }
            return Expense.create(expense)
              .then(function(result) {
                if (result) {
                  return;
                } else {
                  throw new Error('Could not create expense');
                }
              })
              .catch(function(err) {
                throw err;
              });
          })
          .catch(function(err) {
            sails.log.error(err);
            return res.badRequest(err, err.message);
          });

      })
      .then(function(){
        // expenses to update
        console.log("Expenses to update");
        console.log(apiUpdate);
        return apiUpdate;
      })
      .each(function(e) {
        var expense = formatDate(e);
        delete e.createdAt;
        console.log("Updating DB expense");
        console.log(expense);
        return Expense.update({id: expense.id}, expense)
          .then(function(result) {
            if (result) {
              return;
            } else {
              throw new Error('Could not update expense');
            }
          })
          .catch(function(err) {
            sails.log.error(err);
            return res.badRequest(err, err.message);
          });
      })
      .then(function(){

        // Need to re-format dates for parsing on android side
        console.log("Returning expenses to device");
        console.log(devMissing);

        var data = {
          // Expense.create
          apiMissing: apiMissing,
          // Expense.update
          apiUpdate: apiUpdate,
          // return
          devMissing: devMissing,
          // keep track
          devMatched: devMatched,
          // do nothing
          apiMatched: apiMatched
        };

        sails.log.info("------- Summary -------");
        sails.log.info("Device needs to add: "+ devMissing.length);
        sails.log.info("API needs to add:    "+ apiMissing.length);
        sails.log.info("Api needs to update: "+ apiUpdate.length);
        sails.log.info("Api matched:         "+ apiMatched.length);
        sails.log.info("Device matched:      "+ devMatched.length);

        return res.ok(devMissing, "Sync Complete on Api Side");
        // return res.ok(devMissing, "Sync Complete on Api Side");
      })
      .catch(function(err) {
        sails.log.error(err);
        return res.badRequest(err, err.message);
      });

  },

  /**
   * Update an expense
   * @param req
   * @param res
   * @returns {*}
   */
  update: function (req, res) {
    var params = req.allParams();
    sails.log.info('expense/update');
    sails.log.info(params);

    if (!params.id) {
      return res.badRequest({}, 'Missing id parameter');
    }
    if (!params.user) {
      return res.badRequest({}, 'Missing user parameter');
    }
    if (!params.amount) {
      return res.badRequest({}, 'Missing amount parameter');
    }

    Expense.update({id: params.id}, params)
      .then(function(result) {
        if (result && (result.length > 0)) {
          return res.created(result, 'Expense updated');
        } else {
          throw new Error('Could not update expense');
        }
      })
      .catch(function(err) {
        sails.log.error(err);
        return res.badRequest(err, err.message);
      });
  },

  // destroy: function (req, res) {
  //   var params = req.allParams();
  //   sails.log.info('expense/destroy');
  //   sails.log.info(params);
  //   return res.ok();
  // }

};

var formatDates = function(list) {
  var dateFormat = "YYYY-MM-DDThhmmssA"; // 2001-07-04T12:08:23PM
  var result = _.map(list, function(i){
    i.date      = moment.utc(new Date(i.date)).format(dateFormat);
    i.createdAt = moment.utc(new Date(i.createdAt)).format(dateFormat);
    i.updatedAt = moment.utc(new Date(i.updatedAt)).format(dateFormat);
    return i;
  });
  return result;
};

var formatDate = function(e) {
  var dateFormat = "YYYY-MM-DDThhmmssA"; // 2001-07-04T12:08:23PM
  var expense         = {};
  expense.id          = e.id;
  expense.user        = e.user;
  expense.description = e.description;
  expense.amount      = e.amount;
  expense.date        = moment(e.date, dateFormat).format();
  expense.createdAt   = moment(e.createdAt, dateFormat).format();
  expense.updatedAt   = moment(e.updatedAt, dateFormat).format();
  return expense;
}
