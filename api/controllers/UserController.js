/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var moment = require('moment')

module.exports = {

  create: function (req, res) {
    var params = req.allParams();
    sails.log.info('user/create');
    sails.log.info(params);
    return res.ok();
  },

  findOne: function (req, res) {
    var params = req.allParams();
    sails.log.info('user/findOne');
    sails.log.info(params);
    return res.ok();
  },

  find: function (req, res) {
    var params = req.allParams();
    sails.log.info('user/find');
    sails.log.info(params);
    return res.ok();
  },

  update: function (req, res) {
    var params = req.allParams();
    sails.log.info('user/update');
    sails.log.info(params);
    return res.ok();
  },

  destroy: function (req, res) {
    var params = req.allParams();
    sails.log.info('user/destroy');
    sails.log.info(params);
    return res.ok();
  },

  /**
   * Retrieve all the data needed for the dashboard
   * @param req
   * @param res
   * @returns {*}
   */
  // dashboard: function (req, res) {
  //   var params = req.allParams();
  //   sails.log.info('user/dashboard');
  //   sails.log.info(params);
  //
  //   if (!params.id) {
  //     return res.badRequest({}, 'Missing id parameter');
  //   }
  //
  //   var response = {
  //     weeklyLimit: 0,
  //     monthlyLimit: 0,
  //     spentWeek: 0,
  //     spentMonth: 0,
  //     spentYear: 0,
  //     graphWeek: [0,0,0,0,0,0,0],
  //     graphMonth: [0,0,0,0,0,0,0,0,0,0,0,0],
  //     data: []
  //   };
  //
  //   User.findOne({id: params.id})
  //     .populate('expenses')
  //     .then(function(user){
  //       if (!user) {
  //         throw new Error('Could not find user');
  //       } else {
  //         // Save values from user
  //         response.weeklyLimit = user.weeklyLimit;
  //         response.monthlyLimit = user.monthlyLimit;
  //         response.data = _.map(user.budgets, function (item) {
  //           delete item.createdAt;
  //           delete item.updatedAt;
  //           return item;
  //         });
  //         return;
  //       }
  //     })
  //     .then(function(){
  //       // Amount spent this week
  //       // var start   = moment().startOf('week').subtract(7, 'days').utc();
  //       // var end     = moment().startOf('week').utc();
  //       var start = moment().startOf('week').utc();
  //       var end   = moment().startOf('week').add(7, 'days').utc();
  //
  //       sails.log.info('');
  //       sails.log.info('Today\'s Date: \t' +  new Date());
  //       sails.log.info('Start of Week: \t' + start.format('YYYY-MM-DD'));
  //       sails.log.info('End of Week: \t\t' + end.format('YYYY-MM-DD'));
  //
  //       return Budget.find({user: params.id, date: {'>': start.toDate(), '<' : end.toDate()} })
  //         .then(function(budgets){
  //           // Summary - how much spent this week
  //           response.spentWeek = _.reduce(_.pluck(budgets, 'amount'), function(x, y) {
  //             return x + y;
  //             }, 0);
  //           sails.log.info("This Week Spent: \t" + response.spentWeek);
  //
  //           // Graph - how much spent each day of the week
  //           sails.log.info('');
  //           _.each(budgets, function(budget){
  //             var index = moment(budget.date).day() - 1;
  //             response.graphWeek[index] += budget.amount
  //             sails.log.info('Day of Week: \t\t' + index);
  //             sails.log.info('Spent this day: \t' + budget.amount);
  //           });
  //           return;
  //         })
  //         .catch(function(err){
  //           sails.log.error(err);
  //           return res.badRequest(err, err.message);
  //         })
  //     })
  //     .then(function(){
  //       // Amount spent each month of this year
  //       var year = moment().year();
  //       var start = moment().year(year).startOf('year');
  //       var end   = moment().year(year).startOf('year').add(1, 'year');
  //
  //       sails.log.info('');
  //       sails.log.info('This Year: \t\t' + year);
  //       sails.log.info('Start of Year: \t' + start.format('YYYY-MM-DD'));
  //       sails.log.info('End of Year: \t\t' + end.format('YYYY-MM-DD'));
  //
  //       return Budget.find({user: params.id, date: {'>': start.toDate(), '<' : end.toDate()} })
  //         .then(function(budgets){
  //           // Summary - how much spent this year
  //           response.spentYear = _.reduce(_.pluck(budgets, 'amount'), function(x, y) {
  //             return x + y;
  //           }, 0);
  //           sails.log.info("This Year Spent: \t" + response.spentYear);
  //
  //           // Graph - how much spent each month of the year
  //           sails.log.info('');
  //           _.each(budgets, function(budget){
  //             var index = moment(budget.date).month();
  //             response.graphMonth[index] += budget.amount
  //             sails.log.info('Month of Year: \t\t' + index);
  //             sails.log.info('Spent this month: \t' + budget.amount);
  //           });
  //
  //           // Summary - how much spend this month
  //           sails.log.info('');
  //           var month = moment().month();
  //           sails.log.info('This Month: \t\t\t' + month);
  //           response.spentMonth = response.graphMonth[month];
  //           sails.log.info("This Month Spent: \t" + response.spentMonth);
  //
  //           return;
  //         })
  //         .catch(function(err){
  //           sails.log.error(err);
  //           return res.badRequest(err, err.message);
  //         })
  //     })
  //     .then(function(){
  //       return res.ok(response, 'Sending dashboard data')
  //     })
  //     .catch(function(err){
  //       sails.log.error(err);
  //       return res.badRequest(err, err.message);
  //     })
  // },
  //
  // updateWeekBudget: function (req, res) {
  //   var params = req.allParams();
  //   sails.log.info('user/updateWeekBudget');
  //   sails.log.info(params);
  //
  //   if (!params.id) {
  //     return res.badRequest({}, 'Missing id parameter');
  //   }
  //   if (!params.weeklyLimit) {
  //     return res.badRequest({}, 'Missing weeklyLimit parameter');
  //   }
  //
  //   User.update({id: params.id}, {weeklyLimit: params.weeklyLimit})
  //     .then(function(result){
  //       if (result && (result.length > 0)) {
  //         return res.ok(result[0], 'User\'s weekly budget updated');
  //       } else {
  //         throw new Error('Could not update user\'s weekly budget');
  //       }
  //     })
  //     .catch(function(err){
  //       sails.log.error(err);
  //       return res.badRequest(err, err.message);
  //     })
  // },

  updateMonthBudget: function (req, res) {
    var params = req.allParams();
    sails.log.info('user/updateMonthBudget');
    sails.log.info(params);

    if (!params.id) {
      return res.badRequest({}, 'Missing id parameter');
    }
    if (!params.monthlyLimit) {
      return res.badRequest({}, 'Missing monthlyLimit parameter');
    }

    User.update({id: params.id}, {monthlyLimit: params.monthlyLimit})
      .then(function(result){
        if (result && (result.length > 0)) {
          return res.ok(result[0], 'User\'s monthly expense updated');
        } else {
          throw new Error('Could not update user\'s monthly expense');
        }
      })
      .catch(function(err){
        sails.log.error(err);
        return res.badRequest(err, err.message);
      })
  }

};

