INSERT INTO `budgeteer`.`expense` (id, amount, description, date, user) VALUES ('2', '20.9', 'Tim Horton\'s Mocha', '2017-05-09', '1');
INSERT INTO `budgeteer`.`expense` (id, amount, description, date, user) VALUES ('3', '22.38', 'Shoppers Drug Mart', '2017-05-10', '1');
INSERT INTO `budgeteer`.`expense` (id, amount, description, date, user) VALUES ('4', '14.65', 'Chipotle Chicken Wrap', '2017-05-11', '1');
INSERT INTO `budgeteer`.`expense` (id, amount, description, date, user) VALUES ('5', '4.24', 'London Drugs', '2017-05-07', '1');
INSERT INTO `budgeteer`.`expense` (id, amount, description, date, user) VALUES ('6', '60', 'Compass Card', '2017-05-08', '1');
